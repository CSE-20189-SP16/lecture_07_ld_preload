CC=		gcc
CFLAGS=		-std=gnu99 -Wall -fPIC
SOURCE=		kitten.c
PROGRAMS=	$(SOURCE:.c=)
LIBRARIES=	memcpy.so

all:	$(PROGRAMS) $(LIBRARIES)

%:	%.c
	$(CC) $(CFLAGS) -o $@ $<

%.o:	%.c
	$(CC) $(CFLAGS) -c -o $@ $<

%.so:	%.o
	ld -G -o $@ $<

clean:
	rm -f $(PROGRAMS) $(LIBRARIES) *.o
