#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define CHOMP(s)    (s[strlen(s) - 1] = 0)

int main(int argc, char *argv[]) {
    char line[BUFSIZ];
    char copy[BUFSIZ];

    while (fgets(line, BUFSIZ, stdin)) {
    	CHOMP(line);
    	memcpy(copy, line, strlen(line) + 1);

    	printf("LINE:%s\nCOPY:%s\n", line, copy);
    }

    return EXIT_SUCCESS;
}
